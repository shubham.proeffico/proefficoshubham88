@extends('layout')



@section('content')

<!-- if validation in the controller fails, show the errors -->
@if ($errors->any())
   <div class="alert alert-danger">
     <ul>
     @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
     @endforeach
     </ul>
   </div>
@endif

<div class="container">
     <h2 style="text-align: center; font-weight: bold; font-size:40px;" >Employee Details</h2>
        <form name="myfrom" class="from-group"  method="POST" action="{{url('/add')}}" enctype="multipart/form-data">
        @csrf
          <div class="row jumbotron">

          <div class="col-md-6">
                 <label for="fname">Name:</label>
                 <input type="text" class="form-control"
                     placeholder="Enter Name" name="name" required>
             </div>

             <div class="col-md-6">
                 <label for="email">E-mail:</label>
                 <input type="email" class="form-control"
                     placeholder="Enter E-mail" name="email" required>
             </div>

             <div class="col-md-6">
                 <label for="cno">Contact No:</label>
                 <input type="text" class="form-control"
                     placeholder="Enter Contact No" name="phone_no" required>
             </div>

             <div class="col-md-6">
                 <label for="fname">Address:</label>
                 <input type="text" class="form-control"
                     placeholder="Enter Address" name="address" required>
            </div>
            

             <div class="col-md-6">
                 <label for="fname">Bank Name:</label>
                 <select class="form-control" name= "bank_id" required>
                    <option value="">---select---</option>
                    @foreach ($bank as $bank)
                    <option value="{{ $bank -> bank_id }}">{{ $bank->bank_name }}</option>
                 @endforeach
                </select>
             </div>

             <div class="col-md-6">
                 <label for="fname">Account No:</label>
                 <input type="text" class="form-control"
                     placeholder="Enter Account No" name="acc_no" required>
             </div>

             <div class="col-md-6">
                 <label for="fname">Department ID:</label>
                 <select class="form-control" name= "dep_id" required>
                    <option value="">---select---</option>
                    @foreach ($data as $data)
                    <option value="{{ $data -> dep_id }}">{{ $data->dep_name }}</option>
                 @endforeach
                </select>
                 <!--input type="text" class="form-control"
                     placeholder="Enter Address" name="dep_id" required-->
             </div>

             <div class="col-md-6">
                 <label for="fname">ESI No:</label>
                 <input type="text" class="form-control"
                     placeholder="Enter ESI No" name="esi" required>
             </div>

             <div class="col-md-6">
                 <label for="fname">UAN No:</label>
                 <input type="text" class="form-control"
                     placeholder="Enter ESI No" name="uan_no" required>
             </div>

</div>  

            <!--h2 style="text-align: center; font-weight: bold; font-size:30px;" >Additional Details</h2>
                <form name="myfrom" class="from-group"  method="POST" action="">
                 <div class="row jumbotron">

          <div class="col-md-6">
                 <label for="fname">ID Type:</label>
                 <select class="form-control" name= "type" style="width: 200px" required>
                    <option value="">--Type--</option>
                    <option value="aadhar card">Aadhar Card</option>
                    <option value="pan card">PAN Card</option>
                    <option value="voter id">Voter ID</option>
                    <option value="draving laisens">Draving Laisens</option>
                </select>
                 <input type="text" class="form-control"
                     placeholder="Enter Address" name="type" required->
             </div>

             <div class="col-md-6">
                 <label for="fname">ID No</label>
                 <input type="text" class="form-control"
                     placeholder="Enter Address" name="name">
             </div>

             <div class="col-md-6">
                 <label for="fname">File</label>
                 <input type="file" class="form-control"
                     placeholder="Enter Address" name="file" required>
             </div-->

</div>


             <div class="col-12" style="text-align: center; font-size:100px; ">
    <button type="submit" class="btn btn-primary">Ragister</button>
  </div>
             
</div>

@stop
