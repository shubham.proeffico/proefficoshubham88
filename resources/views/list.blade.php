@extends('layout')

@section('content')

<!DOCTYPE html>
<!-- saved from url=(0044)http://delivery.walkinmart.in/deviceApproval -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="YCF4XkSDsQ0y1eMWLpy0DR6GWmvtSK2sv6HOTelg">
    <meta http-equiv="refresh" content="120">

    <title>WalkinMart</title>

    <!-- Custom fonts for this template -->
    <link href="./WalkinMart_files/all.min.css" rel="stylesheet" type="text/css">
    <link href="./WalkinMart_files/css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./WalkinMart_files/sb-admin-2.min.css" rel="stylesheet">
    <!-- Custom styles for this page -->
    <link href="./WalkinMart_files/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="./WalkinMart_files/jquery.min.js.download"></script>
</head>

<body id="app">
<style>
span.inv-span {
    border: solid 1px #e0dfdf;
    margin: 3px;
}

.topbar {
    height: 6.375rem !important;
    list-style: none;
    font-size: 14px;
}



.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}



.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}

.dropdown:hover .dropbtn {
  background-color: #3e8e41;
}



</style>

  <!-- Page Wrapper -->
  <div id="wrapper">
<img src="./WalkinMart_files/loader.gif" id="gif" style="display: block; margin: 0 auto; width: 100px; visibility: hidden; position: fixed; z-index: 9; left: 50%; top: 35%;">
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar" style="display:none;">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="http://delivery.walkinmart.in/">
<!--        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>-->
        <div class="sidebar-brand-text mx-3">WalkinMart</div>
      </a>

          <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Nav Item - Charts -->
     <!-- Nav Item - Tables -->
      <!--<li class="nav-item  ">
        <a class="nav-link" href="http://delivery.walkinmart.in">
          <i class="fas fa-fw fa-table"></i>
          <span>Dashboard</span></a>
      </li>-->
      <!-- Nav Item - Tables -->
    
      
      
     <li class="nav-item  ">
        <a class="nav-link" href="http://delivery.walkinmart.in/allinvoices">
          <i class="fas fa-fw fa-table"></i>
          <span>Invoice Count</span></a>
      </li>
      
      <hr class="sidebar-divider d-none d-md-block">

      <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/allpurchaseret">
	  <i class="fas fa-fw fa-table"></i>
	  <span>All Purchase Return</span></a>
      </li>
      
      <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/purchase-ret">
	  <i class="fas fa-fw fa-table"></i>
	  <span>PR Manager</span></a>
      </li>
      
      

      <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/allsaleret">
	  <i class="fas fa-fw fa-table"></i>
	  <span>All Sale Return</span></a>
      </li>

      <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/allpurchases">
          <i class="fas fa-fw fa-table"></i>
          <span>All Purchases</span></a>
      </li>
	<li class="nav-item ">
            <a class="nav-link" href="http://delivery.walkinmart.in/total">
                <i class="fas fa-fw fa-table"></i>
		<span>Delivery Status</span>
	    </a>
        </li>
        
        <li class="nav-item ">
            <a class="nav-link" href="http://delivery.walkinmart.in/pendingall">
                <i class="fas fa-fw fa-table"></i>
		<span>All Pending Payments</span>
	    </a>
        </li>

<!--
      <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/most-sold-item">
          <i class="fas fa-fw fa-table"></i>
          <span>Most Sold Item</span></a>
      </li>
-->
       <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/all-category">
          <i class="fas fa-fw fa-table"></i>
          <span>All Category</span></a>
      </li>

       <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/all-customer">
          <i class="fas fa-fw fa-table"></i>
          <span>All Customers</span></a>
      </li><!--
       <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/sub-category">
          <i class="fas fa-fw fa-table"></i>
          <span>Sub Categories</span></a>
      </li>
-->
       <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/all-item">
          <i class="fas fa-fw fa-table"></i>
          <span>All Item</span></a>
      </li><!--
       <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/item-available">
          <i class="fas fa-fw fa-table"></i>
          <span>Item Availablity</span></a>
      </li> 

        <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/form">
          <i class="fas fa-fw fa-table"></i>
          <span>Create Student Record</span></a>
      </li> 

-->

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
        
     <li class="nav-item  ">
        <a class="nav-link" href="http://delivery.walkinmart.in/allinvoices">
          <i class="fas fa-fw fa-table"></i>
          <span>Invoice Count</span></a>
      </li>
          

      <!--<li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/allpurchaseret">
	  <i class="fas fa-fw fa-table"></i>
	  <span>All Purchase Return</span></a>
	  <ul>
	  <li class="nav-item ">
            <a class="nav-link" href="http://delivery.walkinmart.in/purchase-ret">
    	  <i class="fas fa-fw fa-table"></i>
    	  <span>PR Manager</span></a>
      </li>
      </ul>
      </li>-->
      
   
<div class="dropdown">
  <a href="http://delivery.walkinmart.in/allpurchaseret" class="">All Purchase Return</a>
  <div class="dropdown-content">
  <a href="http://delivery.walkinmart.in/purchase-ret" style="color:#4e73df;text-decoration-line: underline;
">Purchase Return</a>
  </div>
</div>
 
        
  
      <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/allsaleret">
	  <i class="fas fa-fw fa-table"></i>
	  <span>All Sale Return</span></a>
      </li>

      
   
<div class="dropdown">
  <a href="http://delivery.walkinmart.in/purchase" class="">All Purchase</a>
  <div class="dropdown-content">
  <a href="http://delivery.walkinmart.in/purchase" style="color:#4e73df;text-decoration-line: underline;
">Purchase</a>
  </div>
</div>
 <li class="nav-item ">
            <a class="nav-link" href="http://delivery.walkinmart.in/total">
                <i class="fas fa-fw fa-table"></i>
		<span>Delivery Status</span>
	    </a>
        </li>
        
        <li class="nav-item ">
            <a class="nav-link" href="http://delivery.walkinmart.in/pendingall">
                <i class="fas fa-fw fa-table"></i>
		<span>All Pending Payments</span>
	    </a>
        </li>
        
               <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/all-category">
          <i class="fas fa-fw fa-table"></i>
          <span>All Category</span></a>
      </li>

       <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/all-customer">
          <i class="fas fa-fw fa-table"></i>
          <span>All Customers</span></a>
      </li>      
       <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/all-item">
          <i class="fas fa-fw fa-table"></i>
          <span>All Item</span></a>
      </li>	 <li class="nav-item ">
        <a class="nav-link" href="http://delivery.walkinmart.in/deviceApproval">
          <i class="fas fa-fw fa-table"></i>
          <span>Device Approval</span></a>
      </li>	 
	<div class="dropdown">
  	<a href="http://delivery.walkinmart.in/ptheader" class="">Purchase In</a>
	  <div class="dropdown-content">
	      <a href="http://delivery.walkinmart.in/ptheader_beta" style="color:#4e73df;text-decoration-line: underline;">Purchase In New</a>
	  </div>
	</div>

  <div class="dropdown">
  <a href="" class="">Employee Management System</a>
  <div class="dropdown-content">
  <a href="{{url('/add')}}" style="color:#4e73df;">Add New Employee</a>
  <!--a href="{{url('/list')}}" style="color:#4e73df;">View Employee List</a-->
  </div>
</div>

                <!-- Sidebar Toggle (Topbar) -->
          <form class="form-inline">
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
              <i class="fa fa-bars"></i>
            </button>
          </form>

         

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="http://delivery.walkinmart.in/deviceApproval#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="http://delivery.walkinmart.in/deviceApproval#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Manager</span>
                <img class="img-profile rounded-circle" src="./WalkinMart_files/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="http://delivery.walkinmart.in/deviceApproval#" data-toggle="modal" data-target="#logoutModal">
                  </a><a class="dropdown-item" href="http://delivery.walkinmart.in/logout" onclick="event.preventDefault();document.getElementById(&#39;logout-form&#39;).submit();">Logout</a>
                    <form id="logout-form" action="http://delivery.walkinmart.in/logout" method="POST" style="display: none;">
                    <input type="hidden" name="_token" value="YCF4XkSDsQ0y1eMWLpy0DR6GWmvtSK2sv6HOTelg">                    </form>
                
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <link href="./WalkinMart_files/select2.min.css" rel="stylesheet">
<script src="./WalkinMart_files/select2.min.js.download"></script>
<!-- Page Heading -->
                      <div class="msg-s alert alert-success d-none">
              <strong>Success!</strong> <span class="operation-success"></span>
            </div>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <div class="row">
                    <div class="col-12">
                         <h4 class="m-0 font-weight-bold text-primary text-center" style="text-decoration: underline;">Logged In Devices</h4>
                    </div>   
                </div>
            </div>
            
                <div class="card-body">
                  <div class="table-responsive">
                    <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                      <div class="row">
                        <div class="col-sm-12 col-md-6">
                          <div class="dataTables_length" id="example_length">
                            <label>Show <select name="example_length" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm">
                              <option value="10">10</option><option value="25">25</option>
                              <option value="50">50</option><option value="100">100</option>
                            </select> entries</label>
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                          <div id="example_filter" class="dataTables_filter">
                            <label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example"></input></label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12"><table id="example" class="table table-bordered dataTable no-footer" width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
                      <thead>
                        <tr role="row">
                          <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="DATE: activate to sort column descending" style="width: 35px;">Emp ID</th>
                          <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="NAME: activate to sort column descending" style="width: 242px;">Name</th>
                          <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="EMAIL: activate to sort column ascending" style="width: 103px;">E-mail</th>
                          <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="CONTACT NUMBER: activate to sort column ascending" style="width: 221px;">Phone No</th>
                          <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="ADDRESS: activate to sort column ascending" style="width: 384px;">Address</th>
                          <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="BANK NAME: activate to sort column ascending" style="width: 301px;">Bank ID</th>
                          <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="ACCOUNT: activate to sort column ascending" style="width: 103px;">Account No</th>
                          <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="DEP ID: activate to sort column ascending" style="width: 221px;">Dep ID</th>
                          <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="ESI NO: activate to sort column ascending" style="width: 221px;">ESI No</th>
                          <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="UAN NO: activate to sort column ascending" style="width: 221px;">UAN No</th>
                          <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="VIEW: activate to sort column ascending" style="width: 301px;">Operation</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $item)

                        <tr role="row" class="odd">
                        <th scope="row" class="sorting_1">{{$item->emp_id}}</th>
                        <td>{{$item->name}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->phone_no}}</td>
                        <td>{{$item->address}}</td>
                        <td>{{$item->bank_id}}</td>
                        <td>{{$item->acc_no}}</td>
                        <td>{{$item->dep_id}}</td>
                        <td>{{$item->esi}}</td>
                        <td>{{$item->uan_no}}</td>
                        <td>
                            <a href="/edit/{{$item->emp_id}}"i class="btn btn-primary btn-sm" style=" width:80px; height: 30px; margin-left: 40px;">Update</a>
                            <a href="/delete/{{$item->emp_id}}"i class="btn btn-danger btn-sm" style=" width:80px; height: 30px; margin-top: 5px; margin-left: 40px;">Delete</a>
                            <!--a href=""i class="btn btn-info btn-sm" style=" width:80px; height: 30px; margin-top: 5px; font-size: 13px;">View Details</a-->
                            <button type="button" class="btn btn-primary" data-toggle="modal" style=" width:80px; height: 30px; margin-top: 5px; margin-left: 40px;" data-target="#exampleModal">Details</button>
                        </td>
                      </tr>
                      @endforeach
                      </tbody>

                    </table>
                  </div>
                </div>

              <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
           <div class="modal-dialog" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                       <h5 class="modal-title" id="exampleModalLabel">Employee Additional Information</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                         </button>
                    </div>

                      <div class="modal-body">
                      <div class="row mt-2">
                          <div class="col-md-6"><label class="labels">ID Type</label><input type="text" class="form-control" value=""></div>

                          <div class="col-md-6"><label class="labels">ID Number</label><input type="text" class="form-control" value=""></div>
                      </div>
                      
                      <div class="row mt-3"> 

                        <div class="col-md-6"><label class="labels" style=" margin-left: 80px; ">ID Proof</label></div>
                        
                        <div class="col-md-6"><img src="" alt="image" width="100px;" height="100px;"></div>
                    
                      </div>

                      <div>
                         <h0 style="font-size: 20px; font-weight: bold;">Second ID<h0>
                      </div>

                      <div class="row mt-4">
                        
                        <div class="col-md-6"><label class="labels">ID Type</label><input type="text" class="form-control" value=""></div>

                        <div class="col-md-6"><label class="labels">ID Number</label><input type="text" class="form-control" value=""></div>

                      </div>
                
                      <div class="row mt-4">

                        <div class="col-md-6"><label class="labels" style=" margin-left: 80px; ">ID Proof</label></div>

                        <div class="col-md-6"><img src="" alt="image" width="100px;" height="100px;"></div>
                    
                      </div>
                </div>
            </div>
        </div>
                    </div>

<!--model end-->
                <div class="row">
                  <div class="col-sm-12 col-md-5">
                    <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 7 of 7 entries</div>
                  </div>
                  <div class="col-sm-12 col-md-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                      <ul class="pagination">
                        <li class="paginate_button page-item previous disabled" id="example_previous">
                          <a href="http://delivery.walkinmart.in/deviceApproval#" aria-controls="example" data-dt-idx="0" tabindex="0" class="page-link">Previous</a>
                        </li>
                        <li class="paginate_button page-item active">
                          <a href="http://delivery.walkinmart.in/deviceApproval#" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">1</a>
                        </li>
                        <li class="paginate_button page-item next disabled" id="example_next">
                          <a href="http://delivery.walkinmart.in/deviceApproval#" aria-controls="example" data-dt-idx="2" tabindex="0" class="page-link">Next</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
                  </div>
                </div>
            
	    </div>
          
<style>
    /* Style the tab */
    thead {
        background-color: #267B9F; 
        background-image: linear-gradient(#267B9F, #15455A);
        color: #fff;
    }
    
    .tab {
      overflow: hidden;
      border: 1px solid #ccc;
      background-color: #f1f1f1;
    }
    
    /* Style the buttons that are used to open the tab content */
    .tab button {
      background-color: inherit;
      float: left;
      border: none;
      outline: none;
      cursor: pointer;
      padding: 14px 16px;
      transition: 0.3s;
    }
    
    /* Change background color of buttons on hover */
    .tab button:hover {
      background-color: #ddd;
    }
    
    /* Create an active/current tablink class */
    .tab button.active {
      background-color: #ccc;
    }
    
    /* Style the tab content */
    .tabcontent {
      display: none;
      padding: 6px 12px;
      border: 1px solid #ccc;
      border-top: none;
    }
    a.tablinks {
	        padding: 15px;
		    border: 1px solid #b1b4bb;
    }
    .tab {
	        min-height: 40px;
    }
    select.custom-select.custom-select-md.form-control.form-control-md {
        min-width: 70px;
    }
</style>
<script src="./WalkinMart_files/jquery.min.js.download"></script>
   <script src="./WalkinMart_files/jquery-1.12.4.js.download"></script> 
   <script src="./WalkinMart_files/jquery-ui.js.download"></script>
   <link rel="stylesheet" href="./WalkinMart_files/jquery-ui.css">
   <link rel="stylesheet" href="./WalkinMart_files/jquery.dataTables.min.css">
    <script src="./WalkinMart_files/jquery.dataTables.min.js.download"></script> 
    <script src="./WalkinMart_files/moment.min.js.download" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
        $(document).ready(function() {
            $('#example').DataTable( {
                "order": [[ 0, "asc" ]]
            } );
            
            $('#example1').DataTable( {
                "order": [[ 0, "asc" ]]
            } );
            
            $('#example2').DataTable( {
                "order": [[ 0, "asc" ]]
            } );
            
        } );
        
        
         $( function() {
	     $("#datepicker").datepicker({
		     dateFormat: "dd-mm-yy", 
		     onSelect: function(dateText) {
		     $(this).change();
	    }
	});
       } );

function changeTab(evt, tabName) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}

$( document ).ready(function() {
    document.getElementById("Totalclick").click();
});

function approve(id, elemnt){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
        var settings = {
          "url": "/devApprove",
          "method": "POST",
          "timeout": 0,
          "headers": {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          "data": {
            "id": id,
            "status": 1,
          }
        };
        
        $.ajax(settings).done(function (response) {
            $(elemnt).hide();
        });
        
}

function unapprove(id, elemnt){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
        var settings = {
          "url": "/devApprove",
          "method": "POST",
          "timeout": 0,
          "headers": {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          "data": {
            "id": id,
            "status": 0,
          }
        };
        
        $.ajax(settings).done(function (response) {
                $(elemnt).closest('tr').addClass('d-none');
                $('.operation-success').html('Device successfully deregistered.');
                $('.msg-s').removeClass('d-none');
                setTimeout(function(){ $('.msg-s').addClass('d-none'); }, 3000);
        });
        
}


$('.close, .cut').on('click', function(){
	    $('#history').hide();
});



</script>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Your Website 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="http://delivery.walkinmart.in/deviceApproval#page-top" style="display: block;">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="http://delivery.walkinmart.in/login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="./WalkinMart_files/bootstrap.bundle.min.js.download"></script>

  <!-- Core plugin JavaScript-->
  <script src="./WalkinMart_files/jquery.easing.min.js.download"></script>

  <!-- Custom scripts for all pages-->
  <script src="./WalkinMart_files/sb-admin-2.min.js.download"></script>

  <!-- Page level plugins -->
  <script src="./WalkinMart_files/jquery.dataTables.min.js(1).download"></script>
  <script src="./WalkinMart_files/dataTables.bootstrap4.min.js.download"></script>

  <script>
    // Call the dataTables jQuery plugin
    jQuery(document).ready(function() {
        jQuery('#dataTable').DataTable();
    });

    jQuery(document).ready(function(){
      $('.loader').hide();   
      $("#invoiceCount").click(function(){
        let from_date = $("#from_date").val();
        let to_date = $("#to_date").val();
        var jsonObjects = {"Action":"INV_COUNT","TO": from_date,"FROM": to_date} 
        $.ajax({
              url: "https://live.shreemarutinandan.com/SoftgenAPIPC/MartAPIServices.asmx/Invoices",
              type: "post",
              data: {'jsn': JSON.stringify(jsonObjects) },
              dataType: "json",
              beforeSend: function() {
                  $('#invoiceCount').hide();   
                  $('.loader').show();   
              },
              success: function(result) {
                $('#invoiceCount').show();   
                $('.loader').hide();   
                if(result.status == "success"){
                  let totalInvoice = result.listdata.length;
                  let resText = `Total invoice between ${from_date} to ${to_date} is `+totalInvoice;

                  $('.response').text(resText);
                }
              },
              error: function (request, status, error) {
                $('.response').text(request.responseText);
              }
        }); 
      });
  });
  
  </script>
  <script type="text/javascript">
	// var _0x180e=['#to_date','#invoiceCount','json','https://live.shreemarutinandan.com/SoftgenAPIPC/MartAPIServices.asmx/Invoices','post','stringify','click','#from_date','error','val','listdata','.response','ready','html'];(function(_0x54aa51,_0x180ea4){var _0x27431c=function(_0x4a605f){while(--_0x4a605f){_0x54aa51['push'](_0x54aa51['shift']());}};_0x27431c(++_0x180ea4);}(_0x180e,0x1c7));var _0x2743=function(_0x54aa51,_0x180ea4){_0x54aa51=_0x54aa51-0x0;var _0x27431c=_0x180e[_0x54aa51];return _0x27431c;};jQuery(document)[_0x2743('0x5')](function(){$(_0x2743('0x8'))[_0x2743('0xd')](function(){var _0x216272=$(_0x2743('0x0'))[_0x2743('0x2')](),_0x4a7dab=$(_0x2743('0x7'))[_0x2743('0x2')](),_0x5983dc={'Action':'INV_COUNT','TO':_0x216272,'FROM':_0x4a7dab};$['ajax']({'url':_0x2743('0xa'),'type':_0x2743('0xb'),'data':{'jsn':JSON[_0x2743('0xc')](_0x5983dc)},'dataType':_0x2743('0x9'),'success':function(_0x13a90a){if(_0x13a90a['status']=='success'){var _0x3169e5=_0x13a90a[_0x2743('0x3')]['length'];$(_0x2743('0x4'))[_0x2743('0x6')](_0x3169e5);}else $(_0x2743('0x4'))['html'](_0x13a90a[_0x2743('0x1')]);}});});});
	$('#subgroupid').change(function() {
		// set the window's location property to the value of the option the user has selected
		window.location = $(this).val();
	});
</script> 




</body></html>

@stop