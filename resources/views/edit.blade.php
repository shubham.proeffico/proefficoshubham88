@extends('layout')



@section('content')

<div class="container">
     <h1 style="text-align: center; font-weight: bold; font-size:40px;" >Update Employee Details</h1>
        <form name="myfrom" class="from-group"  method="POST" action="{{ url('update',$data->emp_id) }}">
        @csrf
          <div class="row jumbotron">
          <div class="col-md-6">
                 <label for="fname">Name:</label>
                 <input type="text" class="form-control"
                     placeholder="Enter Name" value="{{$data->name}}" name="name" required>
             </div>

             <div class="col-md-6">
                 <label for="email">E-mail:</label>
                 <input type="email" class="form-control"
                     placeholder="Enter E-mail" value="{{$data->email}}" name="email" required>
             </div>

             <div class="col-md-6">
                 <label for="cno">Contact No:</label>
                 <input type="text" class="form-control"
                     placeholder="Enter Contact No" value="{{$data->phone_no}}" name="phone_no" required>
             </div>

             <div class="col-md-6">
                 <label for="fname">Address:</label>
                 <input type="text" class="form-control"
                     placeholder="Enter Address" value="{{$data->address}}" name="address" required>
             </div>

             <div class="col-md-6">
                 <label for="fname">Bank Name:</label>
                 <select class="form-control" name= "bank_id" style="width: 200px" required>
                 <option value="">---select---</option>
                    @foreach ($bank as $bank)
                    <option value="{{ $bank -> bank_id }}">{{ $bank->bank_name }}</option>
                 @endforeach
                </select>
             </div>

             <div class="col-md-6">
                 <label for="fname">Account No:</label>
                 <input type="text" class="form-control"
                     placeholder="Enter Account No" value="{{$data->acc_no}}" name="acc_no" required>
             </div>

             <div class="col-md-6">
                 <label for="fname">Department Name:</label>
                 <select class="form-control" name= "dep_id" style="width: 200px" required>
                 <option value="">---select---</option>
                    @foreach ($dep as $dep)
                    <option value="{{ $dep -> dep_id }}">{{ $dep->dep_name }}</option>
                   @endforeach
                </select>       
             </div>

             <div class="col-md-6">
                 <label for="fname">ESI NO:</label>
                 <input type="text" class="form-control"
                     placeholder="Enter ESI No" value="{{$data->esi}}" name="esi" required>
             </div>

             <div class="col-md-6">
                 <label for="fname">UAN NO:</label>
                 <input type="text" class="form-control"
                     placeholder="Enter UAN No" value="{{$data->uan_no}}" name="uan_no" required>
             </div>

             

</div>
</div>

             <div class="col-12" style="text-align: center; font-size:80px;">
    <button type="submit" class="btn btn-primary">Update</button>
  </div>
             
</div>
@stop