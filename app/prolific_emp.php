<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class prolific_emp extends Model
{
    protected $table='prolific_emp';
    protected $primaryKey='emp_id';
}

