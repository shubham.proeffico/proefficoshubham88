<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmpController;
use App\Http\Controllers\Empinfo;
use App\Http\Controllers\Dep_nameController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Dropdown

//Route::get('/Dropdown','EmpController@Dropdown');

//home
Route::get('/home','EmpController@index');

//Add
//Route::get('/add','add');
Route::post('/add','EmpController@add');

//View
Route::get('/list','EmpController@list');

//Dropdown
Route::get('/add','EmpController@dep_id');
//Route::get('/','EmpController@bank_id');

//Delete
Route::get('/delete/{emp_id}','EmpController@delete');

//Update
Route::get('edit/{emp_id}','EmpController@edit');
Route::post('update/{emp_id}','EmpController@update');

//Search
Route::get('search/{name}','EmpController@search');

//Additional
Route::view('/show1','show1');
Route::view('/show','show');
Route::post('/show','Empinfo@Info');


