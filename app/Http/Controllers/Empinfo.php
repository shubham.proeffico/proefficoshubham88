<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\prolific_emp_info;
use App\prolific_emp;

class Empinfo extends Controller
{
    //
    public function Info(Request $req){
        
        // $resto= prolific_emp::find($id);
        $email = session('email');
        $emp_data = DB::select("select * from prolific_emp where email = ?",array($email));
        // dd($emp_data[0]->emp_id);
        $resto = new prolific_emp_info;
        $resto->emp_id = $emp_data[0]->emp_id;
        $resto->type=$req->input('type');
        $resto->id_num=$req->input('id_num');
        
        if($req->hasFile('file')){
            $file = $req->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move('public/upload/image/', $filename);
            $resto->file = $filename;
        
        };
        
        $resto->save();

                return response()->redirectTo('show1');
                
        $json = array('status_code'=>200,'response_code'=>1,'message'=>"News List",'response_data'=>$resto);
          return \Response::json($json);
    }

}
