<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class prolific_emp_info extends Model
{
    protected $table='prolific_emp_info';
    protected $fillable = ["type","name", "file", "created_at", "updated_at"];
    protected $primaryKey='emp_id';
}
