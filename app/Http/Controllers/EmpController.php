<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


use App\prolific_emp;
use App\prolific_emp_info;
use App\prolific_dep;
use App\prolific_bank;

class EmpController extends Controller

{
    public function index()
    {
        return view('home');
    }
 
    //DropDown 
    public function dep_id()
    {
        $data = prolific_dep::all ();
        $data1 = prolific_bank::all ();
         return view('add',["data"=>$data , "bank"=>$data1]);

        //$json = array('status_code'=>200,'response_code'=>1,'message'=>"News List",'response_data'=>$data);
			//return \Response::json($json);
        
        }



    //View Data
    public function list(){

    $data =DB::table('prolific_emp')
            ->join('prolific_emp_info','prolific_emp_info.emp_id','=','prolific_emp.emp_id')
            ->select('prolific_emp.emp_id','prolific_emp.name','prolific_emp.email','prolific_emp.phone_no','prolific_emp.address',
            'prolific_emp.bank_id','prolific_emp.acc_no','prolific_emp.dep_id','prolific_emp.esi','prolific_emp.uan_no',
            'prolific_emp_info.type','prolific_emp_info.id_num','prolific_emp_info.file')
            ->get();
            $dep = prolific_dep::all ();
            $bank = prolific_bank::all ();
                return view('list',["data"=>$data , "dep"=>$dep , "bank"=>$bank]);
                

                //$json = array('status_code'=>200,'response_code'=>1,'message'=>"News List",'response_data'=>$data);
			        //return \Response::json($json);

    }

    //Add data
    public function add(Request $req)
    {
        $data = prolific_dep::all ();

        //return $req->input();
        $resto = new prolific_emp;
        $resto->name=$req->input('name');
        $resto->email=$req->input('email');
        $resto->phone_no=$req->input('phone_no');
        $resto->address=$req->input('address');
        $resto->bank_id=$req->input('bank_id');
        $resto->acc_no=$req->input('acc_no');
        $resto->dep_id=$req->input('dep_id');
        $resto->esi=$req->input('esi');
        $resto->uan_no=$req->input('uan_no');
        $resto->save();
        session(["email"=>$resto->email]);
        return redirect('show');


        //$json = array('status_code'=>200,'response_code'=>1,'message'=>"News List",'response_data'=>$resto);
			//return \Response::json($json);
    }

     //Delete function
    public function delete($emp_id) {
            $prolific_emp = prolific_emp::find($emp_id);
            if($prolific_emp)
                 $prolific_emp->delete(); 
                   else
                        return response()->json(error);
                return response()->redirectTo('list');
    }

    //edit data

    public function edit($emp_id){
 
        $data = prolific_emp::find($emp_id);
        $dataa = prolific_dep::all ();
        $dataaa = prolific_bank::all ();
        return view('edit',['data'=>$data , "dep"=>$dataa , "bank"=>$dataaa]);
    }

    //Update

    
    public function update(Request $req , $emp_id){

        $data = prolific_emp::find($emp_id);
        if($data)
        {
            $data->name=$req->input('name');
            $data->email=$req->input('email');
            $data->phone_no=$req->input('phone_no');
            $data->address=$req->input('address');
            $data->bank_id=$req->input('bank_id');
            $data->acc_no=$req->input('acc_no');
            $data->dep_id=$req->input('dep_id');
            $data->esi=$req->input('esi');
            $data->uan_no=$req->input('uan_no');
            $data->update(); 
            return response()->redirectTo('list');

            //return response()->json(['message'=>'Update successfully',], 200);
        
        }else
        {
            return response()->json(['message'=>'No ID'], 200);
        }
    }

    //Search 
    //public function search($name)
    //{
    //    return prolific_emp::where("name",$name)->get();
    //}
    public function search($name) {
        $data = prolific_emp::where("name",$name)
        ->join('prolific_emp_info','prolific_emp_info.emp_id','=','prolific_emp.emp_id')
            ->select('prolific_emp.emp_id','prolific_emp.name','prolific_emp.email','prolific_emp.phone_no','prolific_emp.address',
            'prolific_emp.bank_id','prolific_emp.acc_no','prolific_emp.dep_id','prolific_emp.esi','prolific_emp.uan_no',
            'prolific_emp_info.type','prolific_emp_info.id_num','prolific_emp_info.file')
            ->get();
             return view('list',['data'=>$data]);

            //$json = array('status_code'=>200,'response_code'=>1,'message'=>"News List",'response_data'=>$data);
               //return \Response::json($json);
   }

}
