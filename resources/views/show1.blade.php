
@extends('layout')



@section('content')

<link  rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<div class="container">
     <h2 style="text-align: center; font-weight: bold; font-size:40px; margin-top:50px;" >Additional Details</h2>
        <form name="myfrom" class="from-group"  method="post" action="{{url('/show')}}" enctype= "multipart/form-data">
          @csrf
          <div class="row jumbotron" style="margin-top: 80px;">
            <table  class="table table-hover small-text" id="tb">

              <tr class="tr-header">
                <th>ID Type:</th>
                <th>ID Number:</th>
                <th>Attachment</th>

                <th><a href="javascript:void(0);" style="font-size:18px;" id="addMore" title="Add More Person">
                  <span class="glyphicon glyphicon-plus"></span></a></th>
                  <tr>
                    <div>
                    <td><select name="type" class="form-control" style="height: 35px;" required>
                      <option value="">--Type--</option>
                      <option value="Aadhar Card">Aadhar Card</option>
                      <option value="PAN Card">PAN Card</option>
                      <option value="Voter ID">Voter ID</option>
                      <option value="Draving Laisens">Draving Laisens</option>
                    </select>
                  </td></div>

                  <div><td>
                    <input type="text" name="id_num" class="form-control" required>
                  </td></div>

                    <div><td>
                      <!--input type="file" class="form-control" name="file" required-->
                      <input type="file" name="file" class="form-control" required></td>
                      <td><a href='javascript:void(0);'  class='remove'><span class='glyphicon glyphicon-remove'></span></a>
                      </td></div>
                    </tr>

                  </tr>
                </table>
              </div>
                <div class="col-12" style="text-align:center;">
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>

        </form>
      </div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> 
<script>
$(function(){
    $('#addMore').on('click', function() {
              var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
              data.find("input").val('');
     });
     $(document).on('click', '.remove', function() {
         var trIndex = $(this).closest("tr").index();
            if(trIndex>1) {
             $(this).closest("tr").remove();
           } else {
             alert("Sorry!! Can't remove first row!");
           }
      });
});      
</script>
@stop