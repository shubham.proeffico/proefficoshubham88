<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\http\Controllers\EmpController;
use App\http\Controllers\Empinfo;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
 
//ADD API
Route::post("/add",[EmpController::class,'add']);

//VIEW API
Route::post("/list",[EmpController::class,'list']);

//UPDATE API
Route::post('/update', [EmpController::class,'update']);

Route::get('/edit', [EmpController::class,'edit']);

//DELETE API
Route::delete('/delete/{emp_id}', [EmpController::class,'delete']);

//SEARCH Api
Route::post('search/{name}', [EmpController::class,'search']);

//Additional
Route::post('/show',[Empinfo::class,'info']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});